from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    ShoppingItemListView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
    new_item,
    ShoppingItemListView,
    delete_all_items,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("shopping_items/create/", new_item, name="shopping_item_new"),
    path(
        "shopping_items/",
        ShoppingItemListView.as_view(),
        name="shopping_items_list",
    ),
    path(
        "shopping_items/delete/", delete_all_items, name="shopping_items_delete"
    ),
]
