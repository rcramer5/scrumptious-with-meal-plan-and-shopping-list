from django.urls import path

from meal_plans.views import (
    MealPlanCreateView,
    MealPlanDetailView,
    MealPlanListView,
    MealPlanDeleteView,
    MealPlanUpdateView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplan_list"),
    path(
        "meal_plans/create/", MealPlanCreateView.as_view(), name="mealplan_new"
    ),
    path(
        "meal_plans/<int:pk>/",
        MealPlanDetailView.as_view(),
        name="mealplan_detail",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="mealplan_edit",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="mealplan_delete",
    ),
]
