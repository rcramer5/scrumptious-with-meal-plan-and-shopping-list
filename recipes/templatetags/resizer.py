from django import template
from recipes import views, models

register = template.Library()


@register.filter
def resize_to(ingredient, target):
    # target is new servings, currentServings
    # is number of current servings,
    # ingredient.amount is how large one unit of an ingredient is

    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties

    if ingredient.recipe.servings and target is not None:
        try:
            print(ingredient.recipe.servings)
            print(target)
            ratio = int(target) / ingredient.recipe.servings
            print(ratio)

            return ratio * ingredient.amount
        except:
            print("except")
