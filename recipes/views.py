from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from platformdirs import user_cache_dir
from recipes.forms import RatingForm
from recipes.models import Ingredient, ShoppingItem, FoodItem, Recipe
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        ingredients = []
        for item in self.request.user.shopping_item.all():
            ingredients.append(item.food_item)
        context["servings"] = self.request.GET.get("servings")
        context["food_in_shopping_list"] = ingredients

        return context


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


# shopping items create function
@require_http_methods(["POST"])
def new_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    user = request.user
    ShoppingItem.objects.create(food_item=ingredient.food, user=user)
    # except IntegrityError:
    #     print("Integrity Error thrown")
    #     pass

    return redirect("recipe_detail", pk=ingredient.recipe.id)


# shopping items list function
class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


# shopping items delete function
def delete_all_items(request):
    user = request.user
    ShoppingItem.objects.filter(user=user).delete()
    return redirect("shopping_items_list")
